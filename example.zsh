#!/bin/zsh
function tags() {
   for f in *otl; do
      grep _tag_ $f 2>&1 1>/dev/null && ~/bin/vo_maketags.pl $f
   done
}

function web() {
[[ ! -e web ]] && mkdir web
cd web
rm *.html
for f in ../*otl; do
   ff=$(echo $f | sed 's/\.\.\///g;')
   fff=$(echo $f | sed 's/.otl/.html/g;s/\.\.\///g;')
   title=$(echo $ff | sed 's/.otl//g')
   echo "$f -> $ff -> $fff"
   ../tohtml -v title=$title $f > $fff
done
cp ../style.css .
cat << eof > index.html
<html>
<frameset border=0 frameborder=0 framespacing=0 rows=25,* marginwidth=0>
   <frame name=menue src=./menue.html scrolling=no>
   <frame name=main src=today.html scrolling=auto>
 </frameset>
</html>
eof

cat << eof > menue.html
<html>

<head>
   <base target="main">
   <link href="./style.css" rel=stylesheet type=text/css>
</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class=head>
<table width="100%" border="0" height="100%" cellpadding="0" cellspacing="0"><tr><td valign=left>
	&nbsp;&nbsp;&nbsp;
   [<a href=today.html>Heute</a>] &nbsp;&nbsp;&nbsp;
   </td><td valign=right align=right>
   [<a href="ABC">CBA</a>]
</td></tr></table>
</body>
</html>
eof

REMOTE=USER@HOST:DIR/.

rsync -avP --delete . $REMOTE
}

tags
[[ -z $1 ]] && web
