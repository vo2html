(C)opyright Gerolf Ziegenhain <gerolf@ziegenhain.com> 2008
   GNU GPLv3

Dependencies:
=============
- vim outliner from vimoutliner.org

Possible configuration:
=======================
- pass a title for the page with ... -v title=abc ...
- check out the example.zsh script
